package com.aitekteam.developer.ems;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.aitekteam.developer.ems.adapters.ScreenSlidePagerAdapter;
import com.aitekteam.developer.ems.transformers.ZoomOutPageTransformer;

public class HelperActivity extends AppCompatActivity {

    private ViewPager slider;
    private static final int NUMBER_SLIDER = 6;
    private ActionBar actionBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_helper);

        setSupportActionBar((Toolbar) findViewById(R.id.top_bar));
        this.actionBar = getSupportActionBar();
        this.slider = findViewById(R.id.helper_slider);
        PagerAdapter pagerAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager(), NUMBER_SLIDER);
        this.slider.setAdapter(pagerAdapter);
        this.slider.setPageTransformer(true, new ZoomOutPageTransformer());

        this.setTitle();
    }

    private void setTitle() {
        if (this.actionBar != null) {
            this.actionBar.setDisplayHomeAsUpEnabled(true);
            this.actionBar.setTitle(R.string.tutorial_name);
        }
    }

    @Override
    public void onBackPressed() {
        if (this.slider.getCurrentItem() == 0)
            super.onBackPressed();
        else
            this.slider.setCurrentItem(this.slider.getCurrentItem() - 1);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.slider_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
            case R.id.item_prev:
                if (this.slider.getCurrentItem() != 0)
                    this.slider.setCurrentItem(this.slider.getCurrentItem() - 1);
                break;
            case R.id.item_next:
                if (this.slider.getCurrentItem() != (NUMBER_SLIDER - 1))
                    this.slider.setCurrentItem(this.slider.getCurrentItem() + 1);
                break;
        }
        return true;
    }
}
