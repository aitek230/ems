package com.aitekteam.developer.ems.models.api;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Buildings {

    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public class Datum {

        @SerializedName("id_gedung")
        @Expose
        private Integer idGedung;
        @SerializedName("nama_gedung")
        @Expose
        private String namaGedung;
        @SerializedName("singkatan_gedung")
        @Expose
        private String singkatanGedung;
        @SerializedName("data")
        @Expose
        private List<Datum_> data = null;

        public Integer getIdGedung() {
            return idGedung;
        }

        public void setIdGedung(Integer idGedung) {
            this.idGedung = idGedung;
        }

        public String getNamaGedung() {
            return namaGedung;
        }

        public void setNamaGedung(String namaGedung) {
            this.namaGedung = namaGedung;
        }

        public String getSingkatanGedung() {
            return singkatanGedung;
        }

        public void setSingkatanGedung(String singkatanGedung) {
            this.singkatanGedung = singkatanGedung;
        }

        public List<Datum_> getData() {
            return data;
        }

        public void setData(List<Datum_> data) {
            this.data = data;
        }

        public class Datum_ {

            @SerializedName("Va")
            @Expose
            private Double va;
            @SerializedName("Vb")
            @Expose
            private Double vb;
            @SerializedName("Vc")
            @Expose
            private Double vc;
            @SerializedName("Vab")
            @Expose
            private Double vab;
            @SerializedName("Vbc")
            @Expose
            private Double vbc;
            @SerializedName("Vca")
            @Expose
            private Double vca;
            @SerializedName("Va_now")
            @Expose
            private Double vaNow;
            @SerializedName("Vb_now")
            @Expose
            private Double vbNow;
            @SerializedName("Vc_now")
            @Expose
            private Double vcNow;
            @SerializedName("Ia")
            @Expose
            private Double ia;
            @SerializedName("Ib")
            @Expose
            private Double ib;
            @SerializedName("Ic")
            @Expose
            private Double ic;
            @SerializedName("Ia_now")
            @Expose
            private Double iaNow;
            @SerializedName("Ib_now")
            @Expose
            private Double ibNow;
            @SerializedName("Ic_now")
            @Expose
            private Double icNow;
            @SerializedName("Pa")
            @Expose
            private Double pa;
            @SerializedName("Pb")
            @Expose
            private Double pb;
            @SerializedName("Pc")
            @Expose
            private Double pc;
            @SerializedName("Pa_now")
            @Expose
            private Double paNow;
            @SerializedName("Pb_now")
            @Expose
            private Double pbNow;
            @SerializedName("Pc_now")
            @Expose
            private Double pcNow;
            @SerializedName("Qa")
            @Expose
            private Double qa;
            @SerializedName("Qb")
            @Expose
            private Double qb;
            @SerializedName("Qc")
            @Expose
            private Double qc;
            @SerializedName("Sa")
            @Expose
            private Double sa;
            @SerializedName("Sb")
            @Expose
            private Double sb;
            @SerializedName("Sc")
            @Expose
            private Double sc;
            @SerializedName("PFa")
            @Expose
            private Double pFa;
            @SerializedName("PFb")
            @Expose
            private Double pFb;
            @SerializedName("PFc")
            @Expose
            private Double pFc;
            @SerializedName("PFa_now")
            @Expose
            private Double pFaNow;
            @SerializedName("PFb_now")
            @Expose
            private Double pFbNow;
            @SerializedName("PFc_now")
            @Expose
            private Double pFcNow;
            @SerializedName("Freq")
            @Expose
            private Double freq;
            @SerializedName("TAE")
            @Expose
            private Double tAE;
            @SerializedName("timestamp")
            @Expose
            private String timestamp;

            public Double getVa() {
                return va;
            }

            public void setVa(Double va) {
                this.va = va;
            }

            public Double getVb() {
                return vb;
            }

            public void setVb(Double vb) {
                this.vb = vb;
            }

            public Double getVc() {
                return vc;
            }

            public void setVc(Double vc) {
                this.vc = vc;
            }

            public Double getVab() {
                return vab;
            }

            public void setVab(Double vab) {
                this.vab = vab;
            }

            public Double getVbc() {
                return vbc;
            }

            public void setVbc(Double vbc) {
                this.vbc = vbc;
            }

            public Double getVca() {
                return vca;
            }

            public void setVca(Double vca) {
                this.vca = vca;
            }

            public Double getVaNow() {
                return vaNow;
            }

            public void setVaNow(Double vaNow) {
                this.vaNow = vaNow;
            }

            public Double getVbNow() {
                return vbNow;
            }

            public void setVbNow(Double vbNow) {
                this.vbNow = vbNow;
            }

            public Double getVcNow() {
                return vcNow;
            }

            public void setVcNow(Double vcNow) {
                this.vcNow = vcNow;
            }

            public Double getIa() {
                return ia;
            }

            public void setIa(Double ia) {
                this.ia = ia;
            }

            public Double getIb() {
                return ib;
            }

            public void setIb(Double ib) {
                this.ib = ib;
            }

            public Double getIc() {
                return ic;
            }

            public void setIc(Double ic) {
                this.ic = ic;
            }

            public Double getIaNow() {
                return iaNow;
            }

            public void setIaNow(Double iaNow) {
                this.iaNow = iaNow;
            }

            public Double getIbNow() {
                return ibNow;
            }

            public void setIbNow(Double ibNow) {
                this.ibNow = ibNow;
            }

            public Double getIcNow() {
                return icNow;
            }

            public void setIcNow(Double icNow) {
                this.icNow = icNow;
            }

            public Double getPa() {
                return pa;
            }

            public void setPa(Double pa) {
                this.pa = pa;
            }

            public Double getPb() {
                return pb;
            }

            public void setPb(Double pb) {
                this.pb = pb;
            }

            public Double getPc() {
                return pc;
            }

            public void setPc(Double pc) {
                this.pc = pc;
            }

            public Double getPaNow() {
                return paNow;
            }

            public void setPaNow(Double paNow) {
                this.paNow = paNow;
            }

            public Double getPbNow() {
                return pbNow;
            }

            public void setPbNow(Double pbNow) {
                this.pbNow = pbNow;
            }

            public Double getPcNow() {
                return pcNow;
            }

            public void setPcNow(Double pcNow) {
                this.pcNow = pcNow;
            }

            public Double getQa() {
                return qa;
            }

            public void setQa(Double qa) {
                this.qa = qa;
            }

            public Double getQb() {
                return qb;
            }

            public void setQb(Double qb) {
                this.qb = qb;
            }

            public Double getQc() {
                return qc;
            }

            public void setQc(Double qc) {
                this.qc = qc;
            }

            public Double getSa() {
                return sa;
            }

            public void setSa(Double sa) {
                this.sa = sa;
            }

            public Double getSb() {
                return sb;
            }

            public void setSb(Double sb) {
                this.sb = sb;
            }

            public Double getSc() {
                return sc;
            }

            public void setSc(Double sc) {
                this.sc = sc;
            }

            public Double getPFa() {
                return pFa;
            }

            public void setPFa(Double pFa) {
                this.pFa = pFa;
            }

            public Double getPFb() {
                return pFb;
            }

            public void setPFb(Double pFb) {
                this.pFb = pFb;
            }

            public Double getPFc() {
                return pFc;
            }

            public void setPFc(Double pFc) {
                this.pFc = pFc;
            }

            public Double getPFaNow() {
                return pFaNow;
            }

            public void setPFaNow(Double pFaNow) {
                this.pFaNow = pFaNow;
            }

            public Double getPFbNow() {
                return pFbNow;
            }

            public void setPFbNow(Double pFbNow) {
                this.pFbNow = pFbNow;
            }

            public Double getPFcNow() {
                return pFcNow;
            }

            public void setPFcNow(Double pFcNow) {
                this.pFcNow = pFcNow;
            }

            public Double getFreq() {
                return freq;
            }

            public void setFreq(Double freq) {
                this.freq = freq;
            }

            public Double getTAE() {
                return tAE;
            }

            public void setTAE(Double tAE) {
                this.tAE = tAE;
            }

            public String getTimestamp() {
                return timestamp;
            }

            public void setTimestamp(String timestamp) {
                this.timestamp = timestamp;
            }

        }

    }

}