package com.aitekteam.developer.ems;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.aitekteam.developer.ems.adapters.BuildingsAdapter;
import com.aitekteam.developer.ems.helpers.APIClient;
import com.aitekteam.developer.ems.helpers.DBViewModel;
import com.aitekteam.developer.ems.interfaces.APIInterfaces;
import com.aitekteam.developer.ems.interfaces.CustomOnClickHandler;
import com.aitekteam.developer.ems.models.api.Buildings;
import com.aitekteam.developer.ems.models.datas.DataBuilding;
import com.aitekteam.developer.ems.models.entities.Users;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private ActionBar actionBar;
    private TextView power, energy;
    private LinearLayout gotoContainer;
    private RecyclerView lists;
    private APIInterfaces apiInterfaces;
    private DBViewModel db;
    private BuildingsAdapter adapter;
    private RelativeLayout emptyView;

    private Users profile;
    private Observer<Users> liveProfile = new Observer<Users>() {
        @Override
        public void onChanged(Users users) {
            if (users != null) {
                profile = users;
                getTotal(profile);
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setSupportActionBar((Toolbar) findViewById(R.id.top_bar));
        this.actionBar = getSupportActionBar();
        this.power = findViewById(R.id.statusbar_power);
        this.energy = findViewById(R.id.statusbar_energy);
        this.gotoContainer = findViewById(R.id.statusbar_goto_container);
        this.lists = findViewById(R.id.lists);
        this.db = new DBViewModel(getApplication());
        this.apiInterfaces = APIClient.getClient().create(APIInterfaces.class);
        this.emptyView = findViewById(R.id.empty_view);

        this.setTitle();

        this.lists.setLayoutManager(new LinearLayoutManager(this));
        this.adapter = new BuildingsAdapter(new CustomOnClickHandler() {
            @Override
            public void onClick(Object object, int position, int action) {
                DataBuilding item = (DataBuilding) object;
                Intent intent = new Intent(getApplicationContext(), DetailActivity.class);
                intent.putExtra("selectedBuilding", item);
                startActivity(intent);
            }
        });
        this.lists.setAdapter(this.adapter);

        // Set Up Live Profile
        LiveData<Users> liveDataProfile = this.db.liveProfile();
        liveDataProfile.observe(this, liveProfile);

        this.gotoContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), DisplayBuildingsActivity.class);
                startActivity(intent);
            }
        });
    }

    private void setTitle() {
        if (this.actionBar != null) {
            this.actionBar.setTitle(R.string.dashboard_name);
        }
    }

    private void getTotal(Users profile) {
        if (profile != null) {
            Call<Buildings> buildingsCall = apiInterfaces.getBuildings("day", this.profile.getToken());
            buildingsCall.enqueue(new Callback<Buildings>() {
                @Override
                public void onResponse(Call<Buildings> call, Response<Buildings> response) {
                    Buildings data = response.body();
                    if (data != null) {
                        double kwh = 0, daya = 0;
                        ArrayList<DataBuilding> dataBuildings = new ArrayList<>();
                        for (Buildings.Datum item: data.getData()) {
                            DataBuilding newDataBuilding = new DataBuilding(
                                    item.getIdGedung(),
                                    item.getNamaGedung() + " " + item.getSingkatanGedung(),
                                    0,0
                            );
                            if (item.getData().size() > 0) {
                                ArrayList<Double> arrKWH, arrDaya;
                                arrKWH = new ArrayList<>();
                                arrDaya = new ArrayList<>();
                                for (Buildings.Datum.Datum_ child: item.getData()) {
                                    int timestamp = Integer.parseInt(child.getTimestamp());
                                    if (arrDaya.size() > timestamp) {
                                        arrDaya.set(timestamp,
                                            (arrDaya.get(timestamp) + child.getPaNow() + child.getPbNow() + child.getPcNow()));
                                    }
                                    else {
                                        arrDaya.add((child.getPaNow() + child.getPbNow() + child.getPcNow()));
                                    }

                                    if (arrKWH.size() > timestamp) {
                                        arrKWH.set(timestamp,
                                            (arrKWH.get(timestamp) + child.getTAE()));
                                    }
                                    else {
                                        arrKWH.add((child.getTAE()));
                                    }
                                }

                                daya += arrDaya.get(arrDaya.size() - 1);
                                kwh += arrKWH.get(arrKWH.size() - 1);

                                newDataBuilding.setBuilding_power(
                                        arrDaya.get(arrDaya.size() - 1)
                                );
                                newDataBuilding.setBuilding_energy(
                                        arrKWH.get(arrKWH.size() - 1)
                                );
                            }

                            dataBuildings.add(newDataBuilding);
                        }

                        String satuanDaya = " watt";
                        if (daya > 1000) {
                            daya = daya / 1000;
                            satuanDaya = " kW";
                        }
                        power.setText(new StringBuilder().append(daya).append(satuanDaya));
                        energy.setText(new StringBuilder().append(kwh).append(" kWh"));
                        adapter.setData(dataBuildings);
                        emptyView.setVisibility(View.GONE);
                    }
                    else {
                        power.setText("Not available");
                        energy.setText("00.00 kWh");
                        emptyView.setVisibility(View.VISIBLE);
                    }
                }

                @Override
                public void onFailure(Call<Buildings> call, Throwable t) {
                    power.setText("Not available");
                    energy.setText("00.00 kWh");
                    emptyView.setVisibility(View.VISIBLE);
                }
            });
        }
    }

    private void showTutotrial() {
        Intent intent = new Intent(getApplicationContext(), HelperActivity.class);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.helper_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.item_helper) {
            this.showTutotrial();
        }
        return super.onOptionsItemSelected(item);
    }
}
