package com.aitekteam.developer.ems.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.aitekteam.developer.ems.R;
import com.aitekteam.developer.ems.interfaces.CustomOnClickHandler;
import com.aitekteam.developer.ems.models.datas.DataBuilding;

import java.util.ArrayList;

public class BuildingsAdapter extends RecyclerView.Adapter<BuildingsAdapter.ViewHolder> {

    private ArrayList<DataBuilding> data;
    private CustomOnClickHandler handler;

    public BuildingsAdapter(CustomOnClickHandler handler) {
        this.data = new ArrayList<>();
        this.handler = handler;
    }

    public void setData(ArrayList<DataBuilding> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_building, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        DataBuilding item = this.data.get(position);
        holder.name.setText(item.getBuilding_name());
        String satuanPower = " watt";
        if (item.getBuilding_power() > 1000) {
            item.setBuilding_power((item.getBuilding_power() / 1000));
            satuanPower = " kW";
        }
        holder.power.setText(new StringBuilder().append(item.getBuilding_power()).append(satuanPower));
        holder.energy.setText(new StringBuilder().append(item.getBuilding_energy()).append(" kWh"));
    }

    @Override
    public int getItemCount() {
        return this.data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView name, power, energy;
        ViewHolder(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.item_name);
            power = itemView.findViewById(R.id.item_power);
            energy = itemView.findViewById(R.id.item_energy);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    handler.onClick(data.get(getAdapterPosition()), getAdapterPosition(), 0);
                }
            });
        }
    }
}
