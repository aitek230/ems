package com.aitekteam.developer.ems.interfaces;

public interface CustomOnClickHandler {
    void onClick(Object object, int position, int action);
}
