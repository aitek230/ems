package com.aitekteam.developer.ems.helpers;

import android.app.Application;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.aitekteam.developer.ems.models.AppDatabases;
import com.aitekteam.developer.ems.models.entities.Users;
import com.aitekteam.developer.ems.models.queries.UsersQuery;

public class DBViewModel extends AndroidViewModel {

    public static int ACTION_INSERT = 0;
    public static int ACTION_DELETE = 1;
    public static int ACTION_EDIT = 2;

    public static String TB_USER = "users";

    private UsersQuery usersQuery;

    public DBViewModel(@NonNull Application application) {
        super(application);
        AppDatabases db = AppDatabases.getInstance(application);
        this.usersQuery = db.usersQuery();
    }

    public Users getProfile() {return this.usersQuery.loadProfile();}
    public LiveData<Users> liveProfile() {
        return this.usersQuery.liveProfile();
    }
    public boolean checkAlreadyInstaller() {
        return this.usersQuery.checkProfile() > 0;
    }

    public void doAction(Object data, int action, String table) {
        if (table.equalsIgnoreCase(TB_USER)) {
            if (action == ACTION_INSERT) usersQuery.insert((Users) data);
            else if (action == ACTION_EDIT) usersQuery.update((Users) data);
            else if (action == ACTION_DELETE) usersQuery.delete((Users) data);
        }
        else {
            Log.d("TB_NOT_FOUND", "DATABASE NOT FOUND");
        }
    }

}
