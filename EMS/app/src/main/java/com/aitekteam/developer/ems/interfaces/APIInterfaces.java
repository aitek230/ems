package com.aitekteam.developer.ems.interfaces;

import com.aitekteam.developer.ems.models.api.Buildings;
import com.aitekteam.developer.ems.models.api.Floors;
import com.aitekteam.developer.ems.models.api.Rooms;
import com.aitekteam.developer.ems.models.api.SignInRequest;
import com.aitekteam.developer.ems.models.api.User;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface APIInterfaces {
    @POST("api/auth/signin")
    Call<User> signIn(@Body SignInRequest request);

    @GET("api/admin/umum/data-device/gedung/{type}?")
    Call<Buildings> getBuildings(@Path("type") String type, @Query("api_token") String api_token);

    @GET("api/admin/umum/data-device/gedung/id/{type}/{id_gedung}?")
    Call<Floors> getFloors(@Path("type") String type, @Path ("id_gedung") int id_gedung, @Query("api_token") String api_token);

    @GET("api/admin/umum/data-device/lantai/id/{type}/{id_lantai}?")
    Call<Rooms> getRooms(@Path("type") String type, @Path ("id_lantai") int id_lantai, @Query("api_token") String api_token);
}
