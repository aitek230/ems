package com.aitekteam.developer.ems.adapters;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.aitekteam.developer.ems.fragments.SliderFragment;

public class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
    private int NUMBER_SLIDER;
    public ScreenSlidePagerAdapter(@NonNull FragmentManager fm, int num_slider) {
        super(fm);
        this.NUMBER_SLIDER = num_slider;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        Fragment fragment = new SliderFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("data_slider", position);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public int getCount() {
        return this.NUMBER_SLIDER;
    }
}
