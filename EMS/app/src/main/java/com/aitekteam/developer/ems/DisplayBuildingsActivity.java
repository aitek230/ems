package com.aitekteam.developer.ems;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.aitekteam.developer.ems.helpers.APIClient;
import com.aitekteam.developer.ems.helpers.DBViewModel;
import com.aitekteam.developer.ems.interfaces.APIInterfaces;
import com.aitekteam.developer.ems.models.api.Buildings;
import com.aitekteam.developer.ems.models.datas.DataBuilding;
import com.aitekteam.developer.ems.models.entities.Users;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import lecho.lib.hellocharts.model.Axis;
import lecho.lib.hellocharts.model.AxisValue;
import lecho.lib.hellocharts.model.Line;
import lecho.lib.hellocharts.model.LineChartData;
import lecho.lib.hellocharts.model.PointValue;
import lecho.lib.hellocharts.view.LineChartView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DisplayBuildingsActivity extends AppCompatActivity {

    private ActionBar actionBar;
    private TextView power, energy;
    private LinearLayout gotoContainer;
    private ImageView gotoIcon;
    private LineChartView lineChart;
    private APIInterfaces apiInterfaces;
    private DBViewModel db;
    private ProgressBar loader;

    private Users profile;
    private Observer<Users> liveProfile = new Observer<Users>() {
        @Override
        public void onChanged(Users users) {
            if (users != null) {
                profile = users;
                getTotal("day", profile);
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_buildings);

        setSupportActionBar((Toolbar) findViewById(R.id.top_bar));
        this.actionBar = getSupportActionBar();
        this.power = findViewById(R.id.statusbar_power);
        this.energy = findViewById(R.id.statusbar_energy);
        this.gotoContainer = findViewById(R.id.statusbar_goto_container);
        this.gotoIcon = findViewById(R.id.statusbar_goto);
        this.lineChart = findViewById(R.id.line_chart);
        this.db = new DBViewModel(getApplication());
        this.apiInterfaces = APIClient.getClient().create(APIInterfaces.class);
        this.loader = findViewById(R.id.loader);

        this.setTitle();

        // Set Up Live Profile
        LiveData<Users> liveDataProfile = this.db.liveProfile();
        liveDataProfile.observe(this, liveProfile);

        this.gotoContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(DisplayBuildingsActivity.this);
                builder.setTitle(R.string.label_filter)
                    .setItems(R.array.items_timeline, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            profile = db.getProfile();
                            switch (which) {
                                case 0:
                                    getTotal("day", profile);
                                    break;
                                case 1:
                                    getTotal("month", profile);
                                    break;
                                case 2:
                                    getTotal("year", profile);
                                    break;
                            }
                        }
                    });
                AlertDialog dialog = builder.create();
                builder.show();
            }
        });
    }

    private void setTitle() {
        if (this.actionBar != null) {
            // Enable the Up button
            this.actionBar.setDisplayHomeAsUpEnabled(true);
            this.actionBar.setTitle(R.string.total_name);
            this.gotoIcon.setImageResource(R.mipmap.ic_search);
        }
    }

    private void getTotal(String type, Users profile) {
        if (profile != null) {
            loader.setVisibility(View.VISIBLE);
            Call<Buildings> buildingsCall = apiInterfaces.getBuildings(type, this.profile.getToken());
            buildingsCall.enqueue(new Callback<Buildings>() {
                @Override
                public void onResponse(Call<Buildings> call, Response<Buildings> response) {
                    Buildings data = response.body();
                    if (data != null) {
                        double kwh = 0, daya = 0;
                        String[] xAxisData = null;
                        ArrayList<ArrayList<Double>> linesY = new ArrayList<>();
                        linesY.add(new ArrayList<>());
                        linesY.add(new ArrayList<>());
                        for (Buildings.Datum item: data.getData()) {
                            if (item.getData().size() > 0) {
                                ArrayList<Double> arrKWH, arrDaya;
                                arrKWH = new ArrayList<>();
                                arrDaya = new ArrayList<>();
                                for (Buildings.Datum.Datum_ child: item.getData()) {
                                    int timestamp = Integer.parseInt(child.getTimestamp());
                                    if (arrDaya.size() > timestamp) {
                                        arrDaya.set(timestamp,
                                                (arrDaya.get(timestamp) + child.getPaNow() + child.getPbNow() + child.getPcNow()));
                                    }
                                    else {
                                        arrDaya.add((child.getPaNow() + child.getPbNow() + child.getPcNow()));
                                    }

                                    if (arrKWH.size() > timestamp) {
                                        if (child.getTAE() != null)
                                            arrKWH.set(timestamp,
                                                    (arrKWH.get(timestamp) + child.getTAE()));
                                        else arrKWH.set(timestamp,
                                                (arrKWH.get(timestamp) + 0.00));
                                    }
                                    else {
                                        if (child.getTAE() != null)
                                            arrKWH.add((child.getTAE()));
                                        else
                                            arrKWH.add((0.00));
                                    }
                                }

                                daya += arrDaya.get(arrDaya.size() - 1);
                                kwh += arrKWH.get(arrKWH.size() - 1);
                                xAxisData = new String[arrKWH.size()];

                                for(int i = 0; i < arrKWH.size(); i++) {
                                    if (type.equals("day"))
                                        xAxisData[i] = "Jam " + (i);
                                    if (type.equals("month"))
                                        xAxisData[i] = "Tgl " + (i + 1);
                                    if (type.equals("year"))
                                        xAxisData[i] = "Bulan " + (i + 1);
                                }

                                int lineType = 0;
                                for (ArrayList<Double> yAxisData: linesY) {
                                    if (lineType == 0) {
                                        if (yAxisData.size() > 0) {
                                            int position = 0;
                                            for (double itemDaya : arrDaya) {
                                                yAxisData.set(position, yAxisData.get(position) + itemDaya);
                                                position++;
                                            }
                                        } else {
                                            yAxisData.addAll(arrDaya);
                                        }
                                    }
                                    else {
                                        if (yAxisData.size() > 0) {
                                            int position = 0;
                                            for (double itemEnergy : arrKWH) {
                                                yAxisData.set(position, yAxisData.get(position) + itemEnergy);
                                                position++;
                                            }
                                        } else {
                                            yAxisData.addAll(arrKWH);
                                        }
                                    }
                                    lineType++;
                                }
                            }
                        }

                        String satuanDaya = " watt";
                        if (daya > 1000) {
                            daya = daya / 1000;
                            satuanDaya = " kW";
                        }
                        power.setText(new StringBuilder().append(daya).append(satuanDaya));
                        energy.setText(new StringBuilder().append(kwh).append(" kWh"));

                        List axisValues = new ArrayList();
                        if (xAxisData != null)
                            for (int i = 0; i < xAxisData.length; i++)
                                axisValues.add(i, new AxisValue(i).setLabel(xAxisData[i]));

                        List lines = new ArrayList();
                        for (ArrayList<Double> yAxisData: linesY) {
                            List yAxisValues = new ArrayList();
                            int i = 0;
                            for (Double itemY: yAxisData) {
                                if (itemY > 1000)
                                    itemY = itemY / 1000;
                                yAxisValues.add(new PointValue(i, new BigDecimal(itemY).floatValue()));
                                i++;
                            }
                            if (lines.size() == 0) {
                                lines.add(new Line(yAxisValues).setColor(Color.parseColor("#F25641")));
                            }
                            else {
                                lines.add(new Line(yAxisValues).setColor(Color.parseColor("#C044EB")));
                            }
                        }
                        Axis axis = new Axis();
                        axis.setValues(axisValues);
                        Axis yAxis = new Axis();
                        axis.setTextColor(Color.parseColor("#000000"));
                        yAxis.setTextColor(Color.parseColor("#000000"));
                        LineChartData lineChartData = new LineChartData();
                        lineChartData.setAxisXBottom(axis);
                        lineChartData.setAxisYLeft(yAxis);
                        lineChartData.setLines(lines);
                        lineChart.setLineChartData(lineChartData);
                    }
                    loader.setVisibility(View.GONE);
                }

                @Override
                public void onFailure(Call<Buildings> call, Throwable t) {
                    loader.setVisibility(View.GONE);
                }
            });
        }
    }

    private void showTutotrial() {
        Intent intent = new Intent(getApplicationContext(), HelperActivity.class);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.helper_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.item_helper) {
            this.showTutotrial();
        }
        return super.onOptionsItemSelected(item);
    }
}
