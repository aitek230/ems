package com.aitekteam.developer.ems.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.aitekteam.developer.ems.R;

public class SliderFragment extends Fragment {
    private int data_slider = 0;
    private ImageView image;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        this.data_slider = getArguments().getInt("data_slider", 0);
        return inflater.inflate(R.layout.fragment_slider, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        image = view.findViewById(R.id.slider_image);
        switch (this.data_slider) {
            case 0:
                image.setImageResource(R.drawable.slider_1);
                break;
            case 1:
                image.setImageResource(R.drawable.slider_2);
                break;
            case 2:
                image.setImageResource(R.drawable.slider_3);
                break;
            case 3:
                image.setImageResource(R.drawable.slider_4);
                break;
            case 4:
                image.setImageResource(R.drawable.slider_5);
                break;
            default:
                image.setImageResource(R.drawable.slider_6);
                break;
        }
    }
}
