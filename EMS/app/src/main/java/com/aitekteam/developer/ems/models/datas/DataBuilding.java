package com.aitekteam.developer.ems.models.datas;

import java.io.Serializable;

public class DataBuilding implements Serializable {
    private String building_name;
    private double building_power, building_energy;
    private int building_id;

    public DataBuilding() {}

    public DataBuilding(int building_id, String building_name, double building_power, double building_energy) {
        this.building_id = building_id;
        this.building_name = building_name;
        this.building_power = building_power;
        this.building_energy = building_energy;
    }

    public int getBuilding_id() {
        return building_id;
    }

    public void setBuilding_id(int building_id) {
        this.building_id = building_id;
    }

    public String getBuilding_name() {
        return building_name;
    }

    public void setBuilding_name(String building_name) {
        this.building_name = building_name;
    }

    public double getBuilding_power() {
        return building_power;
    }

    public void setBuilding_power(double building_power) {
        this.building_power = building_power;
    }

    public double getBuilding_energy() {
        return building_energy;
    }

    public void setBuilding_energy(double building_energy) {
        this.building_energy = building_energy;
    }
}
