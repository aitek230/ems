package com.aitekteam.developer.ems.models;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.aitekteam.developer.ems.models.entities.Users;
import com.aitekteam.developer.ems.models.queries.UsersQuery;

@Database(entities = {Users.class}, version = 1, exportSchema = false)
public abstract class AppDatabases extends RoomDatabase {
    private static AppDatabases INSTANCE;
    public abstract UsersQuery usersQuery();

    public static synchronized AppDatabases getInstance(Context context) {
        if (INSTANCE == null) {
            INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                    AppDatabases.class, "DB_EMS").allowMainThreadQueries().build();
        }
        return INSTANCE;
    }

    public static void destroyInstance() {
        INSTANCE = null;
    }
}
