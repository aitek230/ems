package com.aitekteam.developer.ems;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.aitekteam.developer.ems.adapters.BuildingsAdapter;
import com.aitekteam.developer.ems.helpers.APIClient;
import com.aitekteam.developer.ems.helpers.DBViewModel;
import com.aitekteam.developer.ems.interfaces.APIInterfaces;
import com.aitekteam.developer.ems.interfaces.CustomOnClickHandler;
import com.aitekteam.developer.ems.models.api.Floors;
import com.aitekteam.developer.ems.models.datas.DataBuilding;
import com.aitekteam.developer.ems.models.entities.Users;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailActivity extends AppCompatActivity {

    private ActionBar actionBar;
    private TextView power, energy;
    private LinearLayout gotoContainer;
    private DataBuilding building;
    private RecyclerView lists;
    private APIInterfaces apiInterfaces;
    private DBViewModel db;
    private BuildingsAdapter adapter;
    private RelativeLayout emptyView;

    private Users profile;
    private Observer<Users> liveProfile = new Observer<Users>() {
        @Override
        public void onChanged(Users users) {
            if (users != null) {
                profile = users;
                getTotal(profile);
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        setSupportActionBar((Toolbar) findViewById(R.id.top_bar));

        this.actionBar = getSupportActionBar();
        this.power = findViewById(R.id.statusbar_power);
        this.energy = findViewById(R.id.statusbar_energy);
        this.gotoContainer = findViewById(R.id.statusbar_goto_container);
        this.lists = findViewById(R.id.lists);
        this.db = new DBViewModel(getApplication());
        this.apiInterfaces = APIClient.getClient().create(APIInterfaces.class);
        this.emptyView = findViewById(R.id.empty_view);

        this.building = (DataBuilding) getIntent().getSerializableExtra("selectedBuilding");

        this.lists.setLayoutManager(new LinearLayoutManager(this));
        this.adapter = new BuildingsAdapter(new CustomOnClickHandler() {
            @Override
            public void onClick(Object object, int position, int action) {
                DataBuilding item = (DataBuilding) object;
                Intent intent = new Intent(getApplicationContext(), DisplayActivity.class);
                intent.putExtra("selectedFloor", item);
                startActivity(intent);
            }
        });
        this.lists.setAdapter(this.adapter);

        this.setTitle();

        // Set Up Live Profile
        LiveData<Users> liveDataProfile = this.db.liveProfile();
        liveDataProfile.observe(this, liveProfile);

        this.gotoContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (building != null) {
                    Intent intent = new Intent(getApplicationContext(), DisplayFloorsActivity.class);
                    intent.putExtra("selectedBuilding", building);
                    startActivity(intent);
                }
            }
        });
    }

    private void setTitle() {
        if (this.building != null) {
            if (this.actionBar != null) {
                // Enable the Up button
                this.actionBar.setDisplayHomeAsUpEnabled(true);
                this.actionBar.setTitle(this.building.getBuilding_name());
                this.power.setText(new StringBuilder().append(
                        this.building.getBuilding_power()
                ).append(" watt"));
                this.energy.setText(new StringBuilder().append(
                        this.building.getBuilding_energy()
                ).append(" kWh"));
            }
        }
    }

    private void getTotal(Users profile) {
        if (profile != null) {
            Call<Floors> floorsCall = apiInterfaces.getFloors("day", this.building.getBuilding_id(), this.profile.getToken());
            floorsCall.enqueue(new Callback<Floors>() {
                @Override
                public void onResponse(Call<Floors> call, Response<Floors> response) {
                    Floors data = response.body();
                    if (data != null) {
                        double kwh = 0, daya = 0;
                        ArrayList<DataBuilding> dataBuildings = new ArrayList<>();
                        for (Floors.Datum item: data.getData()) {
                            if (item.getData().size() > 0) {
                                Floors.Datum.Datum_ child = item.getData().get(item.getData().size() - 1);
                                kwh += child.getTAE();
                                daya += child.getPaNow() + child.getPbNow() + child.getPcNow();
                                dataBuildings.add(
                                        new DataBuilding(
                                                item.getIdLantai(),
                                                item.getNamaLantai(),
                                                (child.getPaNow() + child.getPbNow() + child.getPcNow()),
                                                child.getTAE()
                                        )
                                );
                            }
                        }

                        String satuanDaya = " watt";
                        if (daya > 1000) {
                            daya = daya / 1000;
                            satuanDaya = " kW";
                        }
                        power.setText(new StringBuilder().append(daya).append(satuanDaya));
                        energy.setText(new StringBuilder().append(kwh).append(" kWh"));
                        adapter.setData(dataBuildings);
                    }
                    else {
                        power.setText("Not available");
                        energy.setText("00.00 kWh");
                        emptyView.setVisibility(View.VISIBLE);
                    }
                }

                @Override
                public void onFailure(Call<Floors> call, Throwable t) {
                    power.setText("Not available");
                    energy.setText("00.00 kWh");
                    emptyView.setVisibility(View.VISIBLE);
                }
            });
        }
    }

    private void showTutotrial() {
        Intent intent = new Intent(getApplicationContext(), HelperActivity.class);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.helper_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.item_helper) {
            this.showTutotrial();
        }
        return super.onOptionsItemSelected(item);
    }
}
