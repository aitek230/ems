package com.aitekteam.developer.ems;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

import com.aitekteam.developer.ems.helpers.DBViewModel;

public class SplashScreenActivity extends AppCompatActivity {

    private DBViewModel db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        this.db = new DBViewModel(getApplication());

        this.checkAlreadyLogin();
    }

    private void checkAlreadyLogin() {
        Intent intent;
        if (this.db.checkAlreadyInstaller()) {
            intent = new Intent(getApplicationContext(), MainActivity.class);
        }
        else {
            intent = new Intent(getApplicationContext(), LoginActivity.class);
        }
        startActivity(intent);
        finish();
    }
}
