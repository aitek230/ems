package com.aitekteam.developer.ems;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.aitekteam.developer.ems.helpers.APIClient;
import com.aitekteam.developer.ems.helpers.DBViewModel;
import com.aitekteam.developer.ems.interfaces.APIInterfaces;
import com.aitekteam.developer.ems.models.api.SignInRequest;
import com.aitekteam.developer.ems.models.api.User;
import com.aitekteam.developer.ems.models.entities.Users;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    private TextView username, password;
    private Button save;
    private APIInterfaces apiInterfaces;
    private DBViewModel db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        this.username = findViewById(R.id.login_username);
        this.password = findViewById(R.id.login_password);
        this.save = findViewById(R.id.login_save);
        this.db = new DBViewModel(getApplication());
        this.apiInterfaces = APIClient.getClient().create(APIInterfaces.class);

        this.save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doLogin();
            }
        });
    }

    private void doLogin() {
        save.setEnabled(false);
        String v_username = this.username.getText().toString();
        String v_password = this.password.getText().toString();
        if (TextUtils.isEmpty(v_username) || TextUtils.isEmpty(v_password)) {
            Toast.makeText(getApplicationContext(), R.string.msg_form_login_cannot_be_null, Toast.LENGTH_LONG).show();
        }
        else {
            SignInRequest request = new SignInRequest();
            request.setUsername(v_username);
            request.setPassword(v_password);
            request.setType("ems");
            Call<User> signIn = apiInterfaces.signIn(request);
            signIn.enqueue(new Callback<User>() {
                @Override
                public void onResponse(Call<User> call, Response<User> response) {
                    User userResponse = response.body();
                    if (userResponse != null) {
                        Users newUser = new Users();
                        newUser.setId_user(userResponse.getData().getIdUser());
                        newUser.setEmail(userResponse.getData().getEmail());
                        newUser.setFull_name(userResponse.getData().getFullName());
                        newUser.setToken(userResponse.getData().getToken());
                        newUser.setApps(userResponse.getData().getApps());
                        newUser.setType(userResponse.getData().getType());
                        db.doAction(newUser, DBViewModel.ACTION_INSERT, DBViewModel.TB_USER);

                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(intent);
                        finish();
                    }
                }

                @Override
                public void onFailure(Call<User> call, Throwable t) {
                    Toast.makeText(getApplicationContext(), R.string.msg_form_login_failed, Toast.LENGTH_LONG).show();
                }
            });
        }
        save.setEnabled(true);
    }
}
